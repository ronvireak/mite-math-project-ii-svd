import matplotlib.pyplot as plt
import numpy as np
from skimage import io, color

#Load the image
image = io.imread('D:\Ron Vireak\SVD Math\img.png')
gray_image = color.rgb2gray(image)

#Compute SVD
U, S, VT = np.linalg.svd(gray_image, full_matrices=False)

#Plot the singular values
plt.figure(figsize=(10, 6))
plt.plot(S, 'bo-', linewidth=2)
plt.title('Singular Values')
plt.xlabel('Index')
plt.ylabel('Value')
plt.show()

#Plot the cumulative sum of singular values
cumulative_sum = np.cumsum(S) / np.sum(S)
plt.figure(figsize=(10, 6))
plt.plot(cumulative_sum, 'ro-', linewidth=2)
plt.title('Cumulative Sum of Singular Values')
plt.xlabel('Index')
plt.ylabel('Cumulative Sum')
plt.show()

#Image compression
def compress_image(U, S, VT, r):
    compressed_image = np.dot(U[:, :r], np.dot(np.diag(S[:r]), VT[:r, :]))
    return compressed_image

#Compress the image with top r singular values
r = 50  # change value r to see the effect of compression
compressed_image = compress_image(U, S, VT, r)

#Display the Original and Compressed image
plt.figure(figsize=(12, 6))

plt.subplot(1, 2, 1)
plt.title('Original Image')
plt.imshow(gray_image, cmap='gray')

plt.subplot(1, 2, 2)
plt.title(f'Compressed Image (r={r})')
plt.imshow(compressed_image, cmap='gray')

plt.show()

# Calculate the sizes
original_size = gray_image.size

# For the compressed image
m, n = gray_image.shape
compressed_size_U = U[:, :r].size
compressed_size_S = S[:r].size
compressed_size_VT = VT[:r, :].size
compressed_size = compressed_size_U + compressed_size_S + compressed_size_VT

# Print the sizes
print(f"Original Image Size: {original_size} elements")
print(f"Compressed Image Size (rank {r}): {compressed_size} elements")

